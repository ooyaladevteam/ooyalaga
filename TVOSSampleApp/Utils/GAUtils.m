//
//  GAUtils.m
//  OoyalaGA
//
//  Created by Piri Koman on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//

#import "GAUtils.h"

Float64 const progressQuarter = 0.25;
Float64 const progressHalf = 0.5;
Float64 const progressThreeQuarters = 0.75;
Float64 const progressEnd = 0.97;

NSString* const MILESTONE_QUARTER = @"playProgressQuarter";
NSString* const MILESTONE_HALF = @"playProgressHalf";
NSString* const MILESTONE_THREEQUARTERS = @"playProgressThreeQuarters";
NSString* const MILESTONE_END = @"playProgressEnd";

OOOoyalaPlayer *_player;
OOOoyalaTVPlayerViewController *_playerController;

@implementation GATracker

+ (id)sharedInstance {
    static GATracker *gaTracker = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gaTracker = [[self alloc] init];
    });
    return gaTracker;
}

- (void)prepare:(OOOoyalaPlayer *)player {
    self.player = player;
    self.prevMilestone = @"";
    self.isProgressQuarterSent = false;
    self.isProgressHalfSent = false;
    self.isProgressThreeQuartersSent = false;
    self.isProgressEndSent = false;
    self.isMetadataLoaded = false;
    self.isPlaybackStarted = false;
    self.isPlaybackPaused = false;
    self.eventCache = [[NSMutableArray alloc] init];
    self.prevEvent = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationHandler:)
                                                 name:nil
                                               object:self.player];
}

- (void)readConfig {
    self.metrics = @{};
    self.metadata = @{};
    self.attributes = @{};
    self.dimensions = [[NSMutableDictionary alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"GAConfig" ofType:@"plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) return;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    
    self.category = [dict valueForKey:@"event_category"];
    self.metrics = [[NSDictionary alloc] initWithDictionary:[dict valueForKey:@"custom_metrics"]];
    self.metadata = [[NSDictionary alloc] initWithDictionary:[dict valueForKey:@"custom_dimensions_from_metadata"]];
    self.attributes = [[NSDictionary alloc] initWithDictionary:[dict valueForKey:@"custom_dimensions_from_attributes"]];
}

- (void)setup: (NSString *)tid {
    self.tid = tid;
    self.appName = @"OoyalaGA";
    self.appVersion = @"0.0.1";
    self.MPVersion = @"1";
    self.cid = [[NSUUID UUID] UUIDString];
    self.ua = @"Mozilla/5.0 (Apple TV; CPU iPhone OS 9_0 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13T534YI";
    self.ul = [[NSLocale preferredLanguages] objectAtIndex:0];
    [self readConfig];
}

- (NSString *)getCurrentMilestone {
    
    Float64 currentMilestone = [self.player playheadTime];
    Float64 p = currentMilestone / self.duration;
    
    if (p > progressQuarter && !self.isProgressQuarterSent && [self.prevMilestone isEqualToString:@""]) {
        self.isProgressQuarterSent = true;
        self.prevMilestone = MILESTONE_QUARTER;
        return self.prevMilestone;
    } else if (p > progressHalf && !self.isProgressHalfSent && [self.prevMilestone isEqualToString: MILESTONE_QUARTER]) {
        self.isProgressHalfSent = true;
        self.prevMilestone = MILESTONE_HALF;
        return self.prevMilestone;
    }else if (p > progressThreeQuarters && !self.isProgressThreeQuartersSent && [self.prevMilestone isEqualToString:MILESTONE_HALF]) {
        self.isProgressThreeQuartersSent = true;
        self.prevMilestone = MILESTONE_THREEQUARTERS;
        return self.prevMilestone;
    }else if (p > progressEnd && !self.isProgressEndSent && [self.prevMilestone isEqualToString:MILESTONE_THREEQUARTERS]) {
        self.isProgressEndSent = true;
        self.prevMilestone = MILESTONE_END;
        return self.prevMilestone;
    }
    return @"";
}

- (void)readMetadata {
    if (self.isMetadataLoaded) return;
    self.duration = self.player.currentItem.duration;
    self.isMetadataLoaded = true;
    
    for (NSString *key in [self.attributes allKeys]) {
        if ([key isEqualToString:@"duration"]) {
            long idx = [[self.attributes objectForKey:key] intValue];
            NSString *value = [NSString stringWithFormat:@"%lf", self.player.duration];
            [self.dimensions setObject:value forKey:[NSString stringWithFormat:@"cd%ld", idx]];
        }
        if ([key isEqualToString:@"title"]) {
            long idx = [[self.attributes objectForKey:key] intValue];
            NSString *value = self.player.currentItem.title;
            [self.dimensions setObject:value forKey:[NSString stringWithFormat:@"cd%ld", idx]];
        }
    }
    
    NSDictionary *metadata = self.player.currentItem.metadata;
    if (metadata != nil){
        for (NSString *key in [metadata allKeys]) {
            long idx = [[self.metadata objectForKey:key] intValue];
            if (idx != 0) {
                NSString *value = [metadata objectForKey:key];
                [self.dimensions setObject:value forKey:[NSString stringWithFormat:@"cd%ld", idx]];
            }
        }        
    }
    
    [self purgeEventCache];
    [self report:@"contentReady"];
}

- (void)send:(NSString *)type :(NSDictionary *)params {
    /*
     Generic hit sender to Measurement Protocol
     Consists out of hit type and a dictionary of other parameters
     */
    NSString *endpoint = @"https://www.google-analytics.com/collect?";
    NSString *parameters = [NSString stringWithFormat:@"v=%@&an=%@&tid=%@&av=%@&cid=%@&t=%@&ua=%@&ul=%@",
                            self.MPVersion, self.appName, self.tid, self.appVersion, self.cid,
                            type, self.ua, self.ul];
    
    for ( NSString *key in [params allKeys]) {
        parameters = [NSString stringWithFormat:@"%@&%@=%@", parameters, key, [params objectForKey:key]];
    }
    
    NSString *paramEndcode = [parameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", endpoint, paramEndcode];
    NSURL *url = [NSURL URLWithString:urlString];
    
    //NSLog(@"URL: %@", url);

    NSURLSessionDataTask *sendToGATask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              
      if (response != nil) {
          // print status code
      } else {
          if (error != nil) {
              NSLog(@"Error: %@", error.description);
          }
      }
    }];

    [sendToGATask resume];
}

- (void)event:(NSString *)action :(NSString *)label :(NSDictionary *)metrics {

    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary: @{@"ec" : self.category, @"ea" : action}];
    
    if (label != nil){
        [params setObject:label forKey:@"el"];
    }
    
    //mix custom metrics
    if (metrics != nil) {
        for ( NSString *key in [metrics allKeys]) {
            [params setObject :[metrics objectForKey:key] forKey:key];
        }
    }
    
    //mix custom dimensions
    for ( NSString *key in [self.dimensions allKeys]) {
        [params setObject :[self.dimensions objectForKey:key] forKey:key];
    }

    [self send:@"event" :params];
}

- (void)purgeEventCache {
    for (int i = 0; i < [self.eventCache count]; i++) {
        [self event: [self.eventCache objectAtIndex:i]: self.player.currentItem.title: nil];
    }
}


- (void)report:(NSString *)action {
    
    long idx = [[self.metrics objectForKey:action] intValue];
    if (idx != 0) {
        NSString *key = [NSString stringWithFormat:@"cm%ld", idx];
        [self event: action: nil: [[NSDictionary alloc] initWithObjectsAndKeys: @"1", key, nil] ];
    }
    
    if ([self.prevEvent isEqualToString:action]) return;
    
    self.prevEvent = action;
    
    if ( self.isMetadataLoaded ){
        [self event:action :self.player.currentItem.title :nil];
    } else {
        [self.eventCache addObject:action];
    }
}

- (void)screenView: (NSString *)screenName :(NSDictionary<NSString *, NSString *> *)customParameters {
    /*
     A screenview hit, use screenname
     */
    NSDictionary *params = @{@"cd": screenName};
    if (customParameters != nil) {
        for ( NSString *key in [customParameters allKeys]) {
            [params setValue:[customParameters objectForKey:key] forKey:key];
        }
    }
    [self send:@"screenview" :params];
}

- (id)init {
    if (self = [super init]) {

    }
    return self;
}

- (void)notificationHandler:(NSNotification *)notification {
    
    if ([notification.name isEqualToString:OOOoyalaPlayerTimeChangedNotification]) {
        
        NSString *event = [self getCurrentMilestone];
        if (![event isEqualToString:@""]) {
            [self report:event];
        }
        
        return;
    }
    
    if ([notification.name isEqualToString:OOOoyalaPlayerAdStartedNotification]) {
        [self report:@"adPlayerbackStarted"];
        return;
        
    }
    
    if ([notification.name isEqualToString:OOOoyalaPlayerAdCompletedNotification]) {
        [self report:@"adPlaybackFinished"];
        return;
    }
    
    if ([notification.name isEqualToString:OOOoyalaPlayerPlayStartedNotification]) {
        [self report:@"playbackStarted"];
        self.isPlaybackStarted = true;
        return;
    }
    
    if ([notification.name isEqualToString:OOOoyalaPlayerStateChangedNotification]) {
        
        if (self.isPlaybackPaused && self.isPlaybackPaused){
            self.isPlaybackPaused = false;
            [self report:@"playbackResumed"];
        }
        
        switch (self.player.state) {
            case OOOoyalaPlayerStatePlaying:
                [self readMetadata];
                break;
                
            case OOOoyalaPlayerStatePaused:
                [self report:@"playbackPaused"];
                self.isPlaybackPaused = true;
                break;
        }
                
    }
    
    if ([notification.name isEqualToString:OOOoyalaPlayerPlayCompletedNotification]) {
        [self report:@"playbackFinished"];
        self.isPlaybackStarted = false;
        self.isPlaybackPaused = false;
        return;
    }

    if ([notification.name isEqualToString:OOOoyalaPlayerErrorNotification]) {
        [self report:@"playbackFailed"];
        NSLog(@"Error: %@", self.player.error);
    }
    
}

- (void)dealloc {
    
}

@end

@implementation OoyalaUtils;

+(OOOoyalaTVPlayerViewController *)createPlayer:(NSString *)domain :(NSString *)pcode :(NSString *)embedCode{
    // Create Ooyala ViewController
    _player = [[OOOoyalaPlayer alloc] initWithPcode:pcode domain:[[OOPlayerDomain alloc] initWithString:domain]];
    _playerController = [[OOOoyalaTVPlayerViewController alloc] initWithPlayer:_player];
    //self.playerController.playbackControlsEnabled = NO;
    
    // Load the video
    [_player setEmbedCode:embedCode];
    [_player play];
    
    [[GATracker sharedInstance] prepare: _player];
    [[GATracker sharedInstance] screenView:@"VideoScreen" :nil];
    
    return _playerController;
}

+(void)shutdownPlayer{
    if (_playerController == nil || _player == nil) return;
    if (_player.isPlaying) {
        [[GATracker sharedInstance] report:@"contentAbandoned"];
    }
    [_player.view removeFromSuperview];
    [_playerController removeFromParentViewController];
    [_player destroy];
}

@end
