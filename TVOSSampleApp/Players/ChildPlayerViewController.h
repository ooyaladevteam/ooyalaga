//
//  ChildPlayerViewController.h
//  TVOSSampleApp
//
//  Created by Piri Koman on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//
// This example shows shows a use case in which you want to show the player next to
// other UI in the screen, so the player will not be in fullscreen mode taking all
// your screen space, but it will be sharing the UI with other elements which could
// be actionable too, like buttons.
//
// This is not a verified use case by Ooyala and cause extrange behavior for the player,
// but we provide the example in case you want to use it.

#import <UIKit/UIKit.h>

@class PlayerSelectionOption;
@class OOOoyalaTVPlayerViewController;
@class OOOoyalaPlayer;

@interface ChildPlayerViewController: UIViewController

//@property (nonatomic, strong) OOOoyalaTVPlayerViewController *ooyalaPlayerViewController;
@property (nonatomic, strong) PlayerSelectionOption *option;
//@property (nonatomic, strong) OOOoyalaPlayer *player;
@property (nonatomic, strong) NSString *pcode;
@property (nonatomic, strong) NSString *playerDomain;


@end
