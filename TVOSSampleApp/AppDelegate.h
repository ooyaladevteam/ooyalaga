//
//  AppDelegate.h
//  TVOSSampleApp
//
//  Created by Piri Koman on 28/10/16.
//  Copyright © 2016 Ooyala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

